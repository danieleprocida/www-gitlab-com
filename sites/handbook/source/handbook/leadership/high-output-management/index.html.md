---
layout: handbook-page-toc
title: High Output Management
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction 

At GitLab, one of our favorite books is, “[High Output Management](/handbook/leadership/book-clubs/#high-output-management)” by Andrew Grove. The book provides a comprehensive overview of a manager's role and purpose. Our CEO, Sid, applied many of the concepts covered when partnering with the People team to design management and people practices for GitLab. On this page, we will cover some of the key topics covered in the book and what they mean for people leaders. 

### High Output Management

The central thesis is that a manager’s objective is to increase the output of the work of those on their team. At GitLab, managers are expected to lead their teams to achieve [results](/handbook/values/#results). Therefore, a manager should choose high-leverage activities that have a multiplicative impact on the overall output of the team. GitLab moves fast. There are meetings, issues and MRs to review, 1-1s, informal communication, and much more. One way for a manager to be successful is by providing a clear direction to a team by applying a small amount of the time that yields a tremendous value in terms of the output of the team. 

For example, managers at GitLab can: 
1. [Delegate](handbook/leadership/effective-delegation/) tasks through [Issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html#:~:text=The%20GitLab%20Issue%20Board%20is,Kanban%20or%20a%20Scrum%20board.&text=Issue%20boards%20help%20you%20to,your%20entire%20process%20in%20GitLab). Issue boards can serve as a project management tool to plan, organize, and visualize a workflow for a team. 

2. [Decline Meetings in Favor of Async](/company/culture/all-remote/asynchronous/#how-to-decline-meetings-in-favor-of-async/). Meetings are useful for building rapport and moving projects forward. Managers can be role models of our [bias towards asynchronous communication](/handbook/values/#bias-towards-asynchronous-communication) by declining meetings in favor of async. They can also be role models by only [scheduling meetings](/handbook/communication/#scheduling-meetings/) where it is necessary to review a [concrete proposal](/handbook/values/#make-a-proposal) or to move forward a series of asynchronous discussion points. 

3. [Making decisions](/handbook/leadership/making-decisions/) by applying the best of both hierarchical and consensus on your team. Allow the person on your team that does the work to make the decisions, the [Directly Responsible Individual](/handbook/people-group/directly-responsible-individuals/). 

4. [Elicit peak performance through motivation](/handbook/leadership/build-high-performing-teams/#motivating-others-to-elicit-peak-performance). Managers can improve output on their team by applying motivation, [building trust](/leadership/building-trust/), and training. 

### Application of Individual KPIs

A topic covered in High Output Management is for managers to choose [Key Performance Indicators](/handbook/ceo/kpis/) to help their team achieve results. Managers are responsible for ensuring their teams have KPIs linked to the company [OKRs](/company/okrs/). Managers can also create both team and individual KPIs to measure what gets done. If people leaders set a goal around a desired outcome for their people, the chances of that outcome occurring can be higher; simply because you have committed to managing and measuring direct report progress towards it. 

When you set goals and KPIs with direct reports, make sure they align with your team’s overall strategy and KPIs, which aligns with the organization's overall strategy. 

### Performance Management 

In the book, the author describes applying [async](/handbook/values/#bias-towards-asynchronous-communication) practices to begin performance discussions. Managers use a regularly scheduled [1-1 meeting](/handbook/leadership/1-1/) to discuss performance with a direct report. They prepare a document before the meeting with all of the key points, areas of strength, areas of development, and a plan for the future. This gives the direct report time to digest before the performance discussion. The [synchronous meeting](/handbook/communication/#video-calls) time can be used by the direct report to ask clarifying questions on the feedback. Managers are able to focus their attention on key points rather than covering all the points in one meeting. 

### Training is the Boss’s Job

Every page in our handbook is a [source of learning and development material](/handbook/people-group/learning-and-development/#handbook-first-training-content/). If managers accept that training, along with motivation, are the key ways to improve the performance of direct reports, the way you teach must be closely tied to what you practice. Training should be considered a continuing process rather than a one-time event. Managers fill the role of teacher to their teams. Team members also have valuable skills they can share with their teams and the broader organization. 

As a people leader, consider holding training events and/or record videos that teach your team members important concepts related to functional skill-building. At GitLab, we have [Focus Friday’s](/handbook/communication/#focus-fridays/) that can be used to [take time out to learn](/handbook/people-group/learning-and-development/learning-initiatives/#take-time-out-to-learn-campaign/).  Enable team members to drive training of the team on complex functions through [enablement sessions](/handbook/leadership/building-trust/#host-a-lunch--learn). Also, consider using [LinkedIn Learning](/handbook/people-group/learning-and-development/linkedin-learning/) to focus your team on curated content. During the next team meeting, have team members share feedback on the topics covered. 

Managers can lead training sessions throughout the year, and according to the book, it is considered one of the highest-leverage activities a manager can do. 


